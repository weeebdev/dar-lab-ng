import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Category } from '@dar-lab-ng/api-interfaces';

interface INavItems {
  title: string;
  link: string;
  isDisabled: boolean;
  isHidden: boolean;
}

@Component({
  selector: 'dar-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent {
  navItems: INavItems[] = [
    {
      title: 'Greetings',
      link: '/greetings',
      isDisabled: false,
      isHidden: false,
    },
    {
      title: 'Articles',
      link: '/articles',
      isDisabled: false,
      isHidden: false,
    },
    {
      title: 'Categories',
      link: '/categories',
      isDisabled: false,
      isHidden: false,
    },
  ];

  constructor() {}
}
