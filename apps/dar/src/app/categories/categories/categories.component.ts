import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from '@dar-lab-ng/api-interfaces';
import { Observable } from 'rxjs';

@Component({
  selector: 'dar-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent {
  categories$: Observable<Category[]>;

  constructor(
    private readonly httpClient: HttpClient,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.categories$ = this.httpClient.get<Category[]>(`api/categories`);
  }

  rowClickHandler(category: Category) {
    this.router.navigate(['categories', category.id]);
  }
}
