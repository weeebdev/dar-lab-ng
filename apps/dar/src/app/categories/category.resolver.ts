import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Category } from '@dar-lab-ng/api-interfaces';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class CategoryResolver implements Resolve<Category> {
  constructor(private readonly httpClient: HttpClient) {}

  resolve(route: ActivatedRouteSnapshot) {
    const categoryId = route.params.id;
    return this.httpClient.get<Category>(`api/category/${categoryId}`).pipe(
      catchError(() => {
        // TODO: Redirect to 404
        return of(null);
      })
    );
  }
}
