import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'dar-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public projectName = '';

  constructor() {}

  ngOnInit(): void {
    this.projectName = 'DarLab';
  }
}
