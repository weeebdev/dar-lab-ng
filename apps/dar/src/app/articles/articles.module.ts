import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticlesComponent } from './articles/articles.component';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ArticlesRoutingModule } from './articles-routing.module';
import { ArticleComponent } from './article/article.component';
import { ArticleResolver } from './article.resolver';

@NgModule({
  declarations: [ArticlesComponent, ArticlesListComponent, ArticleComponent],
  imports: [CommonModule, ArticlesRoutingModule],
  providers: [ArticleResolver],
})
export class ArticlesModule {}
