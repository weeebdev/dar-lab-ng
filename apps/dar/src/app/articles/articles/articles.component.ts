import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Article, Category } from '@dar-lab-ng/api-interfaces';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, map, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'dar-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss'],
})
export class ArticlesComponent implements OnInit {
  articles$: Observable<Article[]>;

  constructor(private httpClient: HttpClient, private router: Router) {}

  ngOnInit(): void {
    this.articles$ = this.httpClient.get<Article[]>(`api/articles`).pipe(
      catchError((err) => of(null)),
      mergeMap((articles) =>
        !articles
          ? of(null)
          : this.httpClient.get<Category[]>(`api/categories`).pipe(
              map((categories) => {
                return articles.map((article) => {
                  const category = categories.find(
                    (c) => c.id === article.category_id
                  );
                  return {
                    ...article,
                    category_title: category ? category.title : 'No category',
                  };
                });
              }),
              catchError((err) => of(articles))
            )
      )
    );
  }

  rowClickHandler(article: Article) {
    this.router.navigate(['articles', article.id]);
  }
}
