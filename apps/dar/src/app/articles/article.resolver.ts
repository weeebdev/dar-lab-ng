import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Article } from '@dar-lab-ng/api-interfaces';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ArticleResolver implements Resolve<Article> {
  constructor(private readonly httpClient: HttpClient) {}

  resolve(route: ActivatedRouteSnapshot) {
    const articleId = route.params.id;
    return this.httpClient.get<Article>(`api/articles/${articleId}`).pipe(
      catchError(() => {
        // TODO: Redirect to 404
        return of(null);
      })
    );
  }
}
