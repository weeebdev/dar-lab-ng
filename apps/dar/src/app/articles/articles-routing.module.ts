import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { ArticleResolver } from './article.resolver';
import { ArticleComponent } from './article/article.component';
import { ArticlesComponent } from './articles/articles.component';

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: ArticlesComponent,
  },
  {
    path: ':id',
    component: ArticleComponent,
    resolve: {
      article: ArticleResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArticlesRoutingModule {}
