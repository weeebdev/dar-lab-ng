import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dar-greeting',
  templateUrl: './greeting.component.html',
  styleUrls: ['./greeting.component.scss'],
})
export class GreetingComponent implements OnInit {
  public projectName = '';

  constructor() {}

  ngOnInit(): void {
    this.projectName = 'DarLab';
  }
}
