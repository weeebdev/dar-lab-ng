import { Controller, Get, HttpService, Param, Query } from '@nestjs/common';

import {
  Article,
  ArticleRequest,
  Category,
  Message,
} from '@dar-lab-ng/api-interfaces';

import { AppService } from './app.service';
import { map } from 'rxjs/operators';

const API = 'https://media-api.dar-dev.zone/api';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private httpClient: HttpService
  ) {}

  @Get('hello')
  getData(): Message {
    return this.appService.getData();
  }

  @Get('/articles/:id')
  getArticle(@Param('id') id) {
    return this.httpClient
      .get<Article[]>(`${API}/articles/${id}`)
      .pipe(map((res) => res.data));
  }

  @Get('/articles')
  getArticles() {
    const params: {
      sort: string;
      limit: number;
    } = {
      sort: 'id:DESC',
      limit: 10,
    };

    return this.httpClient
      .get<Article[]>(`${API}/articles`, { params })
      .pipe(map((res) => res.data));
  }

  @Get('/categories')
  getCategories() {
    return this.httpClient
      .get<Category[]>(`${API}/categories`)
      .pipe(map((res) => res.data));
  }

  @Get('/category/:id')
  getCategory(@Param('id') id) {
    return this.httpClient
      .get<Category[]>(`${API}/categories/${id}`)
      .pipe(map((res) => res.data));
  }
}
