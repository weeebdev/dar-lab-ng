export interface Message {
  message: string;
}

export interface Article {
  id: string;
  title: string;
  created_at: string;
  annotation: string;
  category_id: string;
  category_title?: string;
}

export interface Category {
  id: string;
  title: string;
  sort: number;
}

export interface ArticleRequest {
  category_id: string;
}
